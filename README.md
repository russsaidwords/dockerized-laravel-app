# dockerized-laravel-app

Laravel running in a set of docker containers brought up via docker-compose.
This is the logical conclusion after following [this](https://www.digitalocean.com/community/tutorials/how-to-set-up-laravel-nginx-and-mysql-with-docker-compose) how-to article.

## Steps:

1. `git clone git@gitlab.com:russsaidwords/dockerized-laravel-app.git your-project-directory`

2. `cd your-project-directory`

3. `docker run --rm -v $(pwd):/app composer install` - you will want to add the `composer.lock` file to source control

4. `sudo chown -R $USER:$USER your-project-directory`

5. Modify `MYSQL_ROOT_PASSWORD` from `docker-compose.yml` file. 

6. Copy `.env.example` to `.env` - review connection details: "DB_HOST=" must match container_name of mysql container in docker-compose.yml, "DB_PASSWORD=" must match as well.

7. `docker-compose up -d`

8. `docker-compose exec app php artisan key:generate`

9. `docker-compose exec app php artisan config:cache`

10. You can now visit http://127.0.0.1 and see the Laravel default project page.

## Next Steps:

1. See the above linked article for next steps such as creating a new database user for the project database.

2. Happy hacking!
